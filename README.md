# haskelloworld

helloworld in haskell with stack/nix/gitlab-ci configs


## Stack config

```
$ stack setup
...

$ stack build
...

$ stack exec haskelloworld
<!DOCTYPE HTML><html><body><h1>haskelloworld</h1></body></html>

```


## Nix config

```
$ nix-shell --run "cabal run"
...
<!DOCTYPE HTML><html><body><h1>haskelloworld</h1></body></html>
```


## Gitlab-ci config

see `.gitlab-ci.yml`

 [![Build status](https://framagit.org/juliendehos/haskelloworld/badges/master/build.svg)](https://framagit.org/juliendehos/haskelloworld/pipelines)

