{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text.Lazy.IO as LIO
import Lucid

main :: IO ()
main = LIO.putStrLn $ renderText $ doctypehtml_ $ body_ $ h1_ "haskelloworld"

